using System.Xml.Serialization;

namespace DRExporter
{
    [XmlRoot(ElementName = "data")]
    public class DataEntry
    {
        [XmlElement(ElementName = "value")] 
        public string Value { get; set; }
        
        [XmlAttribute(AttributeName = "name")] 
        public string Name { get; set; }                
    }
}