﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using CommandLine;
using CSharpx;

namespace DRExporter
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed(options =>
                {
                    try
                    {
                        Export(options);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                });
        }

        private static void Export(Options options)
        {
            var resXml = CreateResxDocument();

            var entries = GetDataEntries(options);
            foreach (var entry in entries)
            {
                resXml.Root?.Add(Serialize(entry));
            }

            using (var fs = File.Create(options.OutputFile))
            {
                resXml.Save(fs);
            }

            Console.WriteLine(resXml);
        }

        private static DataEntry[] GetDataEntries(Options options)
        {
            var filePaths = options.SearchPatterns
                .SelectMany(pattern => Directory.GetFiles(options.RootDir, pattern, SearchOption.AllDirectories))
                .Select(fileName =>
                {
                    var sb = new StringBuilder(fileName);
                    options.ValueExcludes
                        .ForEach(x => sb.Replace(x, ""));

                    return sb.ToString();
                })
                .Distinct()
                .ToArray();

            return filePaths
                .Select(filePath =>
                {
                    var relativePath = filePath.Substring(options.RootDir.TrimEnd(Path.DirectorySeparatorChar).Length);

                    var className = Path.ChangeExtension(relativePath, null)
                        .Replace(Path.DirectorySeparatorChar, '.')
                        .Trim('.')
                        .Replace(" ", string.Empty);

                    return new DataEntry
                    {
                        Name = className,
                        Value = Path.Combine(
                            Path.GetFileName(options.RootDir.TrimEnd(Path.DirectorySeparatorChar)),
                            relativePath.TrimStart(Path.DirectorySeparatorChar)
                        ).TrimStart(Path.DirectorySeparatorChar)
                    };
                })
                .ToArray();
        }

        private static XDocument CreateResxDocument()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var templateName = assembly.GetManifestResourceNames().Single(x => x.Contains("ResourceTemplate.xml"));

            using (var template = assembly.GetManifestResourceStream(templateName))
            {
                return XDocument.Load(template);
            }
        }

        private static XElement Serialize(DataEntry entry)
        {
            using (var ms = new MemoryStream())
            using (var sw = new StreamWriter(ms))
            {
                var xmlSerializer = new XmlSerializer(typeof(DataEntry));
                var ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                xmlSerializer.Serialize(sw, entry, ns);
                ms.Seek(0, SeekOrigin.Begin);

                return XElement.Load(ms);
            }
        }
    }
}