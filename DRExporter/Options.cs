using System.Collections.Generic;
using CommandLine;

namespace DRExporter
{
    public sealed class Options
    {
        [Option('o', "outputFile", Required = true, HelpText = "Destination of the .resx file to be stored.")]
        public string OutputFile { get; set; }

        [Option('d', "dir", Required = true, HelpText = "Root directory to be scanned for matches.")]
        public string RootDir { get; set; }

        [Option("value-exclude-part", HelpText = "Removes matching string from data value.")]
        public IEnumerable<string> ValueExcludes { get; set; }

        [Option('p', "searchPattern", Default = "*", HelpText = "Search pattern to match files.")]
        public IEnumerable<string> SearchPatterns { get; set; }
    }
}